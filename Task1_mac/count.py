import json
import time
from jsonschema import validate, ValidationError
import asyncio
import aiohttp

time_schema = {
    "type": "object",
    "properties": {
        "sites":
            {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
    }
}


def validate_json(json_data):
    try:
        validate(instance=json_data, schema=time_schema)
        print(json_data)
    except ValidationError as err:
        return False
    return True


class responce_time:
    def __init__(self, site, time):
        self.site = site
        self.time = time

    def __str__(self):
        return f'{self.site} : {self.time}\n'


class responce_count_time:
    def __init__(self):
        self.times = []

    def add_time(self, time):
        self.times.append(time)

    def __str__(self):
        return ''.join([str(time) for time in self.times])



async def fetch(session, url):
    start = time.time()
    try:
        result = await session.get(url)
        end = time.time()
        return {"url": url, "status": result.status, "time": end - start}
    except:
        return {"url": url, "status": "wrong url"}


async def fetch_all(urls, loop):
    async with aiohttp.ClientSession(loop=loop) as session:
        results = await asyncio.gather(*[fetch(session, url) for url in urls], return_exceptions=True)
        return results


def count(event, context):
    print(event)
    json_body = json.loads(event["body"])
    # json_body = event["body"]

    if not validate_json(json_body):
        print("json mistake")
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json'
            },
            'body': "Request body contains mistakes!"
        }

    result = responce_count_time()

    loop = asyncio.get_event_loop()
    responces = loop.run_until_complete(fetch_all(json_body["sites"], loop))

    for responce in responces:
        print(responce)
        if type(responce) == dict:
            if responce['status'] == 200:
                result.add_time(responce_time(responce['url'], responce['time']))
            else:
                result.add_time(responce_time(responce['url'], f"Failed: {responce['status']}"))

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': str(result)
    }

#
# if __name__ == "__main__":
#     test = {
#         "body":
#             {
#                 "sites": ["https://google.com", "https://docs.python.org/get"]
#             }
#     }
#
#     event = json.dumps(test)
#     context = []
#     count(test, context)
